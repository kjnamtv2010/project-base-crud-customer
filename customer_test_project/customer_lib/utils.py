import calendar
import time
import logging
from datetime import datetime
from customer_test_project.customer_lib.constant import DATETIME_FORMAT

logger = logging.getLogger(__name__)


def get_timestamp():
	return int(time.time())


def parse_string_to_date(str_date, dt_format=DATETIME_FORMAT):
	if not str_date:
		return None
	str_date = str_date.strip()
	date_result = None
	try:
		date_result = datetime.strptime(str_date, dt_format)
	except Exception as err:
		logger.error("parse_string_to_date_fail|str_date=%s, err=%s", str_date, err)
	return date_result


def datetime_to_timestamp(dt):
	return calendar.timegm(dt.utctimetuple())


def timestamp_to_string(timestamp, format='%Y-%m-%d %H:%M:%S'):
	dt = datetime.fromtimestamp(int(timestamp))
	return dt.strftime(format)


def is_none_empty_string(str):
	return not str or str.isspace()
