DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"
DATETIME_FORMAT = DATE_FORMAT + " " + TIME_FORMAT

class ToggleType(object):
    OFF = 0
    ON = 1

class UserType(object):
    ADMIN = 0
    CUSTOMER = 1

class Method(object):
    POST = 'POST'
    GET = 'GET'
    PUT = 'PUT'
    DELETE = 'DELETE'

class Result(object):
    SUCCESS = 'success'
    AUTH_FAILED = 'auth_failed'
    ERROR_AUTH_FAILED = 'You_are_missing_access_token_or_access_token_is_not_valid'
    ERROR_VALUE = 'error_value'
    ERROR_OS = 'error_os'
    ERROR_USER_NAME_OR_PASSWORD = 'missing_user_name_or_password'
    ERROR_REQUEST_NOT_VALID = 'request_is_not_valid'
    ERROR_USER_NAME_USED = 'user_name_has_been_used'
    ERROR_WRONG_PASSWORD = 'wrong_password'
    ERROR_MISSING_EVENT_TIME = 'missing_event_time'
    ERROR_CAN_NOT_CREATE_CUSTOMER = 'error_can_not_create_customer'
    ERROR_CAN_NOT_UPDATE_CUSTOMER = 'error_can_not_update_customer'
    ERROR_USER_NOT_EXISTS = 'error_user_not_exists'
    ERROR_CAN_NOT_DELETED_CUSTOMER = 'error_can_not_deleted_customer'
    ERROR_METHOD = 'error_method'


RESULT_MESSAGE_MAPPING = {
    Result.SUCCESS: 'success',
    Result.AUTH_FAILED: 'auth failed',
    Result.ERROR_VALUE: 'error value',
    Result.ERROR_OS: 'error_os',
    Result.ERROR_AUTH_FAILED: 'You are missing access_token or access_token is not valid',
    Result.ERROR_USER_NAME_OR_PASSWORD: 'missing user_name or password',
    Result.ERROR_REQUEST_NOT_VALID: 'request is not valid',
    Result.ERROR_USER_NAME_USED: 'user_name has been used',
    Result.ERROR_WRONG_PASSWORD: 'wrong password',
    Result.ERROR_MISSING_EVENT_TIME: 'missing event time',
    Result.ERROR_CAN_NOT_CREATE_CUSTOMER: 'error can not create customer',
    Result.ERROR_USER_NOT_EXISTS: 'error user not exists',
    Result.ERROR_CAN_NOT_DELETED_CUSTOMER: 'error can not delete customer',
    Result.ERROR_CAN_NOT_UPDATE_CUSTOMER: 'error can not update customer',
    Result.ERROR_METHOD: 'error method'
}
