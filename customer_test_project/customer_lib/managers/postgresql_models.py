# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from customer_test_project.customer_lib.utils import timestamp_to_string


class CustomerTab(models.Model):
    user_name = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.IntegerField(default=1)
    user_type = models.IntegerField(default=1)
    email = models.CharField(max_length=255, blank=True, null=True)
    salt = models.CharField(max_length=255, blank=True, null=True)
    date_of_birthday = models.BigIntegerField()
    create_at = models.BigIntegerField()
    update_at = models.BigIntegerField()

    def date_of_birthday_str(self):
        self.date_of_birthday_str = timestamp_to_string(self.date_of_birthday)
        return self.date_of_birthday_str

    class Meta:
        managed = False
        db_table = 'customer'

