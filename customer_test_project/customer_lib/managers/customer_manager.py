import hashlib
import uuid
from customer_test_project.common.object_support import assign
from customer_test_project.customer_lib.managers.postgresql_models import CustomerTab
from customer_test_project.customer_lib.constant import *
from customer_test_project.customer_lib.utils import *


def check_customer_name_is_used(data):
    return CustomerTab.objects.filter(user_name=data['user_name'])


def create(data):
    try:
        salt = uuid.uuid4().hex
        hashed_password = hashlib.sha512((data['password'] + salt).encode('utf-8')).hexdigest()
        date_of_birthday = datetime_to_timestamp(parse_string_to_date(data['date_of_birthday']))
        customer = CustomerTab(
            password=hashed_password,
            salt=salt,
            date_of_birthday=date_of_birthday,
            is_active=ToggleType.ON,
            user_type=UserType.CUSTOMER,
            create_at=get_timestamp(),
            update_at=get_timestamp(),
        )
        assign(customer, data, ['user_name', 'full_name', 'email', 'address'])
        customer.save()
        return customer.id
    except Exception as err:
        logger.error('error_create_customer| err=%s', err)
        return 0


def get_customer_by_id(id):
    try:
        customer = CustomerTab.objects.filter(pk=id)
        if not customer:
            return
        return customer.first()
    except Exception as err:
        logger.error('error_create_customer| err=%s', err)
        return


def update(data, customer_id):
    try:
        salt = uuid.uuid4().hex
        hashed_password = hashlib.sha512((data['password'] + salt).encode('utf-8')).hexdigest()
        date_of_birthday = datetime_to_timestamp(parse_string_to_date(data['date_of_birthday']))
        updated_customer = CustomerTab.objects.filter(pk=customer_id).update(
            password=hashed_password,
            user_name=data['user_name'],
            full_name=data['full_name'],
            email=data['email'],
            address=data['address'],
            salt=salt,
            date_of_birthday=date_of_birthday,
            is_active=ToggleType.ON,
            user_type=UserType.CUSTOMER,
            create_at=get_timestamp(),
            update_at=get_timestamp(),
        )
        if updated_customer > 0:
            return True
        return False
    except Exception as err:
        logger.error('error_update_customer| err=%s', err)
        return False


def delete_customer(id):
    try:
        deleted_customer = CustomerTab.objects.filter(pk=id).update(is_active=ToggleType.OFF)
        if deleted_customer > 0:
            return True
        return False
    except Exception as err:
        logger.error('error_create_customer| err=%s', err)
        return False


def get_list_customer():
    customers = CustomerTab.objects.filter(
        is_active=ToggleType.ON,
        user_type=UserType.CUSTOMER,
    ).order_by('-id')
    return customers


def serialize_customers(customers):
    result = []
    if customers:
        for customer in customers:
            result.append({
                'full_name': customer.full_name,
                'user_name': customer.user_name,
                'address': customer.address,
                'email': customer.email,
                'birthday': timestamp_to_string(customer.date_of_birthday)
            })
    return result

def verify(data):
    customer = CustomerTab.objects.filter(user_name=data['user_name'])
    if not customer:
        return None
    hashed_password = hashlib.sha512((data['password'] + customer[0].salt).encode('utf-8')).hexdigest()
    if hashed_password != customer[0].password:
        return None
    return customer[0]
