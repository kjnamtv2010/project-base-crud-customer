from django.http import JsonResponse
from httplib2 import Response


def api_response_data(result_code, reply=None):
    if reply:
        return JsonResponse(data=dict(result=result_code, reply=Response(reply)), safe=False)
    return JsonResponse(data=dict(result=result_code), safe=False)
