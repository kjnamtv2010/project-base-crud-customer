import json
from datetime import datetime
from functools import wraps
from jose import jwt

from customer_test_project.common.handle_support import api_response_data
from customer_test_project.customer_lib.constant import Result
from customer_test_project.customer_lib.utils import logger, is_none_empty_string

SECRET_KEY_CUSTOMER = 'secret_customer'


def get_customer_access_token(data):
    return jwt.encode(data, SECRET_KEY_CUSTOMER, algorithm='HS256')


def auth_customer(access_token):
    try:
        data_json = jwt.decode(access_token, SECRET_KEY_CUSTOMER, algorithms=['HS256'])
        data = json.loads(json.dumps(data_json))
        if data['id'] and datetime.strptime(data['expiration_date'], "%Y-%m-%d %H:%M:%S") >= datetime.now():
            return data['id']
    except Exception as err:
        logger.error('error_auth_customer_exception| err=%s', err)
        return 0
    return 0


def login_required(f):
    @wraps(f)
    def wrap(request, index=None, *args, **kwargs):
        headers = request.META
        access_token = headers.get("HTTP_ACCESS_TOKEN")
        if is_none_empty_string(access_token) or auth_customer(access_token) <= 0:
            return api_response_data(Result.AUTH_FAILED)
        if index is not None:
            return f(request, index, *args, **kwargs)
        return f(request, *args, **kwargs)
    return wrap
