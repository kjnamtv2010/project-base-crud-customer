import datetime
import json
from django.forms import model_to_dict
from customer_test_project.common.auth import get_customer_access_token, login_required
from customer_test_project.common.handle_support import api_response_data
from customer_test_project.customer_lib.constant import Result, Method
from customer_test_project.customer_lib.managers import customer_manager
from customer_test_project.customer_lib.utils import is_none_empty_string


def customer_direction_handle(request, index=None):
    if index:
        if request.method == Method.GET:
            return get_customer_details(request, index)
        elif request.method == Method.PUT:
            return update_customer(request, index)
        elif request.method == Method.DELETE:
            return delete_customer(request, index)
        return request
    if request.method == Method.POST:
        return create_customer(request)
    if request.method == Method.GET:
        return get_list_customer(request)
    return request


@login_required
def create_customer(request):
    data = json.loads(request.body)
    if is_none_empty_string(data['user_name']) or is_none_empty_string(data['password']):
        return api_response_data(Result.ERROR_USER_NAME_OR_PASSWORD)
    if customer_manager.check_customer_name_is_used(data):
        return api_response_data(Result.ERROR_USER_NAME_USED)
    customer_id = customer_manager.create(data)
    if customer_id > 0:
        return api_response_data(Result.SUCCESS, dict(customer_id=customer_id))
    return api_response_data(Result.ERROR_CAN_NOT_CREATE_CUSTOMER)


@login_required
def get_customer_details(request, id):
    customer = customer_manager.get_customer_by_id(id)
    if not customer:
        return api_response_data(Result.ERROR_USER_NOT_EXISTS)
    return api_response_data(Result.SUCCESS, model_to_dict(customer))


@login_required
def update_customer(request, id):
    data = json.loads(request.body)
    if is_none_empty_string(data['user_name']) or is_none_empty_string(data['password']):
        return api_response_data(Result.ERROR_USER_NAME_OR_PASSWORD)
    is_updated = customer_manager.update(data, id)
    if not is_updated:
        return api_response_data(Result.ERROR_CAN_NOT_UPDATE_CUSTOMER)
    return api_response_data(Result.SUCCESS)


@login_required
def delete_customer(request, id):
    is_deleted = customer_manager.delete_customer(id)
    if not is_deleted:
        return api_response_data(Result.ERROR_USER_NOT_EXISTS)
    return api_response_data(Result.SUCCESS)


@login_required
def get_list_customer(request):
    customers = customer_manager.get_list_customer()
    customer_dict = customer_manager.serialize_customers(customers)
    return api_response_data(Result.SUCCESS, {'customers': customer_dict})


def login(request):
    data = json.loads(request.body)
    if request.method == 'POST':
        if is_none_empty_string(data['user_name']) or is_none_empty_string(data['password']):
            return api_response_data(Result.ERROR_USER_NAME_OR_PASSWORD)
        customer = customer_manager.verify(data)
        if not customer:
            return api_response_data(Result.AUTH_FAILED)
        access_token = get_customer_access_token({
            'id': customer.id,
            'user_name': customer.user_name,
            'password': customer.password,
            'expiration_date': (datetime.datetime.now() + datetime.timedelta(days=2)).strftime("%Y-%m-%d %H:%M:%S")
        })
        return api_response_data(Result.SUCCESS, dict(access_token=access_token))

